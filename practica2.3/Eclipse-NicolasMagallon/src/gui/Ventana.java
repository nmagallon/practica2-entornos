package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JToolBar;
import javax.swing.JButton;
import javax.swing.JTabbedPane;
import javax.swing.JTextPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JSpinner;
import javax.swing.JRadioButton;
import java.awt.Component;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * 
 * @author Nicol�s
 * @since 20/01/2018
 *
 */

public class Ventana extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana frame = new Ventana();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ventana() {
		setResizable(false);
		setIconImage(Toolkit.getDefaultToolkit().getImage(Ventana.class.getResource("/img/check-form.png")));
		setTitle("Formulario");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnArchivo = new JMenu("Archivo");
		menuBar.add(mnArchivo);
		
		JMenuItem mntmNuevo = new JMenuItem("Nuevo");
		mnArchivo.add(mntmNuevo);
		
		JMenuItem mntmAbrir = new JMenuItem("Abrir");
		mnArchivo.add(mntmAbrir);
		
		JMenuItem mntmGuardar = new JMenuItem("Guardar");
		mnArchivo.add(mntmGuardar);
		
		JMenuItem mntmCerrar = new JMenuItem("Cerrar");
		mnArchivo.add(mntmCerrar);
		
		JMenu mnEditar = new JMenu("Editar");
		menuBar.add(mnEditar);
		
		JMenuItem mntmCopiar = new JMenuItem("Copiar");
		mnEditar.add(mntmCopiar);
		
		JMenuItem mntmCortar = new JMenuItem("Cortar");
		mnEditar.add(mntmCortar);
		
		JMenuItem mntmPegar = new JMenuItem("Pegar");
		mnEditar.add(mntmPegar);
		
		JMenuItem mntmDeshacer = new JMenuItem("Deshacer");
		mnEditar.add(mntmDeshacer);
		
		JMenuItem mntmRehacer = new JMenuItem("Rehacer");
		mnEditar.add(mntmRehacer);
		
		JMenu mnPreferencias = new JMenu("Preferencias");
		mnEditar.add(mnPreferencias);
		
		JMenuItem mntmUsuario = new JMenuItem("Usuario");
		mnPreferencias.add(mntmUsuario);
		
		JMenuItem mntmMemoria = new JMenuItem("Memoria");
		mnPreferencias.add(mntmMemoria);
		
		JMenuItem mntmInterfaz = new JMenuItem("Interfaz");
		mnPreferencias.add(mntmInterfaz);
		
		JMenuItem mntmIdioma = new JMenuItem("Idioma");
		mnPreferencias.add(mntmIdioma);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setBounds(10, 11, 774, 31);
		contentPane.add(toolBar);
		
		JButton btnNuevo = new JButton("Nuevo");
		toolBar.add(btnNuevo);
		
		JButton btnAbrir = new JButton("Abrir");
		toolBar.add(btnAbrir);
		
		JButton btnGuardar = new JButton("Guardar");
		toolBar.add(btnGuardar);
		
		JButton btnCopiar = new JButton("Copiar");
		toolBar.add(btnCopiar);
		
		JButton btnPegar = new JButton("Pegar");
		toolBar.add(btnPegar);
		
		JButton btnDeshacer = new JButton("Deshacer");
		toolBar.add(btnDeshacer);
		
		JButton btnRehacer = new JButton("Rehacer");
		toolBar.add(btnRehacer);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 53, 774, 486);
		contentPane.add(tabbedPane);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Datos personales", null, panel, null);
		panel.setLayout(null);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(26, 22, 60, 14);
		panel.add(lblNombre);
		
		textField = new JTextField();
		textField.setBounds(96, 19, 284, 20);
		panel.add(textField);
		textField.setColumns(10);
		
		JLabel lblApellidos = new JLabel("Apellidos");
		lblApellidos.setBounds(26, 47, 60, 14);
		panel.add(lblApellidos);
		
		textField_1 = new JTextField();
		textField_1.setBounds(96, 44, 284, 20);
		panel.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblEdad = new JLabel("Edad");
		lblEdad.setBounds(26, 72, 46, 14);
		panel.add(lblEdad);
		
		JSpinner spinner = new JSpinner();
		spinner.setBounds(96, 69, 46, 20);
		panel.add(spinner);
		
		JLabel lblSexo = new JLabel("Sexo");
		lblSexo.setBounds(26, 97, 46, 14);
		panel.add(lblSexo);
		
		JRadioButton rdbtnHombre = new JRadioButton("Hombre");
		buttonGroup.add(rdbtnHombre);
		rdbtnHombre.setBounds(91, 93, 72, 23);
		panel.add(rdbtnHombre);
		
		JRadioButton rdbtnMujer = new JRadioButton("Mujer");
		buttonGroup.add(rdbtnMujer);
		rdbtnMujer.setBounds(163, 93, 109, 23);
		panel.add(rdbtnMujer);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Primaria", "Secundaria", "Grado Medio", "Grado Superior", "T\u00EDtulo Universitario"}));
		comboBox.setBounds(96, 119, 284, 20);
		panel.add(comboBox);
		
		JLabel lblEducacin = new JLabel("Educaci\u00F3n");
		lblEducacin.setBounds(26, 122, 60, 14);
		panel.add(lblEducacin);
		
		JCheckBox chckbxVehculoPropio = new JCheckBox("Veh\u00EDculo propio");
		chckbxVehculoPropio.setBounds(21, 146, 215, 23);
		panel.add(chckbxVehculoPropio);
		
		JLabel lblNotas = new JLabel("Notas");
		lblNotas.setBounds(26, 176, 72, 14);
		panel.add(lblNotas);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(26, 201, 715, 186);
		panel.add(scrollPane);
		
		JTextPane textPane = new JTextPane();
		scrollPane.setViewportView(textPane);
		
		JButton btnSalir = new JButton("Salir");
		btnSalir.setBounds(652, 410, 89, 23);
		panel.add(btnSalir);
		
		JButton btnGuardarPerfil = new JButton("Guardar");
		btnGuardarPerfil.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnGuardarPerfil.setBounds(553, 410, 89, 23);
		panel.add(btnGuardarPerfil);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Organizaci\u00F3n", null, panel_1, null);
		panel_1.setLayout(null);
		
		JLabel lblNombre_1 = new JLabel("Nombre");
		lblNombre_1.setBounds(26, 22, 60, 14);
		panel_1.add(lblNombre_1);
		
		textField_2 = new JTextField();
		textField_2.setBounds(96, 19, 284, 20);
		panel_1.add(textField_2);
		textField_2.setColumns(10);
		
		JLabel lblDireccin = new JLabel("Direcci\u00F3n");
		lblDireccin.setBounds(26, 47, 66, 14);
		panel_1.add(lblDireccin);
		
		textField_3 = new JTextField();
		textField_3.setBounds(96, 44, 284, 20);
		panel_1.add(textField_3);
		textField_3.setColumns(10);
		
		JLabel lblTamao = new JLabel("Tama\u00F1o");
		lblTamao.setBounds(26, 72, 46, 14);
		panel_1.add(lblTamao);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setModel(new DefaultComboBoxModel(new String[] {"Peque\u00F1a", "Mediana", "Grande"}));
		comboBox_1.setBounds(96, 69, 284, 20);
		panel_1.add(comboBox_1);
		
		JLabel lblSector = new JLabel("Sector");
		lblSector.setBounds(26, 97, 46, 14);
		panel_1.add(lblSector);
		
		textField_4 = new JTextField();
		textField_4.setBounds(96, 94, 284, 20);
		panel_1.add(textField_4);
		textField_4.setColumns(10);
		
		JLabel lblEmpleados = new JLabel("Empleados");
		lblEmpleados.setBounds(26, 122, 66, 14);
		panel_1.add(lblEmpleados);
		
		JSpinner spinner_1 = new JSpinner();
		spinner_1.setBounds(96, 119, 52, 20);
		panel_1.add(spinner_1);
		
		JLabel lblNotas_1 = new JLabel("Notas");
		lblNotas_1.setBounds(26, 147, 46, 14);
		panel_1.add(lblNotas_1);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(26, 172, 715, 217);
		panel_1.add(scrollPane_1);
		
		JTextPane textPane_1 = new JTextPane();
		scrollPane_1.setViewportView(textPane_1);
		
		JButton btnSalir_1 = new JButton("Salir");
		btnSalir_1.setBounds(652, 410, 89, 23);
		panel_1.add(btnSalir_1);
		
		JButton btnGuardar_1 = new JButton("Guardar");
		btnGuardar_1.setBounds(553, 410, 89, 23);
		panel_1.add(btnGuardar_1);
	}
}
