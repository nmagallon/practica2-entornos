﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibreriaVS_NicolasMagallon;

namespace ProyectoVS
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("El método validarPrimo() pide un número y determina si es primo o no.");
            MetodosNumericos.ValidarPrimo();

            Console.WriteLine("El método numeroAleatorio() pide un rango de números a partir del cual sacar un número aleatorio.");
            MetodosNumericos.NumeroAleatorio();

            Console.WriteLine("El método calcularPotencia() calcula la potencia deseada del número indicado.");
            Console.WriteLine(MetodosNumericos.CalcularPotencia(5, 2));
            Console.ReadKey();

            Console.WriteLine("El método binarioADecimal() calcula el valor decimal de un número binario.");
            Console.WriteLine(MetodosNumericos.BinarioADecimal());
            Console.ReadKey();

            Console.WriteLine("El método esPar() determina si un número es par o no.");
            MetodosNumericos.EsPar();

            Console.WriteLine("El método palabraAleatoria() pide la longitud de la palabra aleatoria para crearla.");
            MetodosString.PalabraAleatoria();

            Console.WriteLine("El método invertirCadena() invierte la cadena introducida.");
            MetodosString.InvertirCadena();

            Console.WriteLine("El método mostrarAbecedario() muestra el abecedario en mayúsculas.");
            MetodosString.MostrarAbecedario();

            Console.WriteLine("El método compararCadenas() compara dos cadenas y devuelve si el resultado es true o false.");
            Console.WriteLine(MetodosString.CompararCadenas("Hola", "Mundo"));
            Console.ReadKey();

            Console.WriteLine("El método dibujarArbol() pide la altura del triángulo a dibujar.");
            MetodosString.DibujarArbol();
            Console.ReadKey();
        }
    }
}
