package metodos;

import java.util.Scanner;

public class MetodosNumericos {
	
	public static void validarPrimo(Scanner input) {
		
		System.out.println("Introduce un n�mero entero.");
		int numeroLeido = input.nextInt();
		int contadorDivisores = 0;
		
		for (int i = 1; i <= numeroLeido; i++) {
			if (numeroLeido % i == 0) {
				contadorDivisores++;
			}
		}
		
		if (contadorDivisores == 2) {
			System.out.println("El n�mero introducido es primo.");
		} else {
			System.out.println("El n�mero introducido no es primo.");
		}
	}
	
	public static void numeroAleatorio(Scanner input) {
		
		System.out.println("Introduce el n�mero m�ximo.");
		int numeroMaximo = input.nextInt();
		System.out.println("Introduce el n�mero m�nimo.");
		int numeroMinimo = input.nextInt();
		
		System.out.println("Tu n�mero aleatorio es " + (numeroMinimo + (int)(Math.random()*(numeroMaximo - numeroMinimo))) + ".");
	}
	
	public static int calcularPotencia(int base, int exponente) {
		
		int resultado = 1;
		
		for (int i = 1; i <= exponente; i++ ) {
			resultado = resultado * base;
		}
		
		return resultado;
	}
	
	public static double binarioADecimal(Scanner input) {
		
		System.out.println("Introduce un n�mero binario.");
		long numeroBinario = Long.parseLong(input.nextLine());
		
		double numeroDecimal = 0;
		int exponente = 0;
		
		while (numeroBinario > 0) {
			numeroDecimal = numeroDecimal + numeroBinario % 10 * (int)Math.pow(2, exponente);
			exponente++;
			numeroBinario /= 10;
		}
		
		return numeroDecimal;
	}
	
	public static void esPar(Scanner input) {
		
		System.out.println("Escribe un n�mero entero.");
		int numero = input.nextInt();
		
		if (numero % 2 == 0) {
			System.out.println(numero + " es un n�mero par.");
		} else {
			System.out.println(numero + "es un n�mero impar.");
		}
	}
}
