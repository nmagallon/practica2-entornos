package metodos;

import java.util.Scanner;

public class MetodosString {

	public static void palabraAleatoria(Scanner input) {
		
		System.out.println("Introduce la longitud de la palabra.");
		int longitud = input.nextInt();
		
		for (int i = 0; i < longitud; i++) {
			System.out.print((char)(Math.random()*('Z' - 'A') + 'A'));
		}
		System.out.println();
	}
	
	public static void invertirCadena(Scanner input) {
		
		System.out.println("Introduce una cadena.");
		String cadenaLeida = input.nextLine();
		
		for(int i = cadenaLeida.length() - 1; i >= 0; i--) {
			System.out.print(cadenaLeida.charAt(i));
		}
		System.out.println();
	}
	
	public static void mostrarAbecedario() {
		
		for (int i = 'A'; i <= 'Z'; i++) {
			if (i == 'O') {
				System.out.println('�');
			}
			System.out.println((char)i);
		}
	}
	
	public static boolean compararCadenas(String cadena1, String cadena2) {
		
		if(cadena1.equals(cadena2)) {
			return true;
		}
		
		return false;
	}
	
	public static void dibujarArbol(Scanner input) {
		
		System.out.println("Introduce la altura del �rbol.");
		int altura = input.nextInt();
		
		for (int i = 1; i <= altura; i++) {
			for (int k = altura - i; k > 0; k--) {
				System.out.print(" ");
			}
			for (int j = (i + (i - 1)); j > 0; j--) {
				System.out.print("*");
			}
			
			System.out.println();
		}
	}
}
