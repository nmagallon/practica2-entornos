﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaVS_NicolasMagallon
{
    public class MetodosNumericos
    {
        public static void ValidarPrimo()
        {
            System.Console.WriteLine("Introduce un número entero.");
            int numeroLeido = int.Parse(System.Console.ReadLine());
            int contadorDivisores = 0;
            for (int i = 1; i <= numeroLeido; i++)
            {
                if (numeroLeido % i == 0)
                {
                    contadorDivisores++;
                }
            }
            if (contadorDivisores == 2)
            {
                System.Console.WriteLine("El número introducido es primo.");
            }
            else
            {
                System.Console.WriteLine("El número introducido no es primo.");
            }
            Console.ReadKey();
        }

        public static void NumeroAleatorio()
        {
            System.Console.WriteLine("Introduce el número máximo.");
            int numeroMaximo = int.Parse(System.Console.ReadLine());
            System.Console.WriteLine("Introduce el número mínimo.");
            int numeroMinimo = int.Parse(System.Console.ReadLine());
            Random random = new Random();
            int numeroAleatorio = random.Next(numeroMinimo, numeroMaximo);
            System.Console.WriteLine("Tu número aleatorio es " + numeroAleatorio);
            Console.ReadKey();
        }

        public static int CalcularPotencia(int numero, int exponente)
        {
            int resultado = 1;

            for (int i = 1; i <= exponente; i++)
            {
                resultado = resultado * numero;
            }

            return resultado;
        }

        public static double BinarioADecimal()
        {

            System.Console.WriteLine("Introduce un número binario.");
            long numeroBinario = long.Parse(System.Console.ReadLine());

            double numeroDecimal = 0;
            int exponente = 0;

            while (numeroBinario > 0)
            {
                numeroDecimal = numeroDecimal + numeroBinario % 10 * (int)Math.Pow(2, exponente);
                exponente++;
                numeroBinario /= 10;
            }

            return numeroDecimal;
        }

        public static void EsPar()
        {

            System.Console.WriteLine("Escribe un número entero.");
            int numero = int.Parse(System.Console.ReadLine());

            if (numero % 2 == 0)
            {
                System.Console.WriteLine(numero + " es un número par.");
            }
            else
            {
                System.Console.WriteLine(numero + "es un número impar.");
            }
            Console.ReadKey();
        }
    }

    public class MetodosString
    {
        public static void PalabraAleatoria()
        {
            System.Console.WriteLine("Introduce la longitud de la palabra.");
            int longitud = int.Parse(System.Console.ReadLine());
            Random random = new Random();
            int letraAleatoria;
            for (int i = 0; i < longitud; i++)
            {
                letraAleatoria = random.Next('A', 'Z');
                System.Console.Write((char)(letraAleatoria));
            }
            Console.ReadKey();
        }

        public static void InvertirCadena()
        {
            System.Console.WriteLine("Introduce una cadena.");
            String cadenaLeida = System.Console.ReadLine();
            for (int i = cadenaLeida.Length - 1; i >= 0; i--)
            {
                System.Console.Write(cadenaLeida[i]);
            }
            Console.ReadKey();
        }

        public static void MostrarAbecedario()
        {
            for (int i = 'A'; i <= 'Z'; i++)
            {
                if (i == 'O')
                {
                    Console.WriteLine('Ñ');
                }
                Console.WriteLine((char)i);
            }
            Console.ReadKey();
        }

        public static Boolean CompararCadenas(String cadena1, String cadena2)
        {
            if (cadena1.Equals(cadena2))
            {
                return true;
            }
            return false;
        }

        public static void DibujarArbol()
        {
            Console.WriteLine("Introduce la altura del árbol.");
            int altura = int.Parse(System.Console.ReadLine());

            for (int i = 1; i <= altura; i++)
            {
                for (int k = altura - i; k > 0; k--)
                {
                    Console.Write(" ");
                }
                for (int j = (i + (i - 1)); j > 0; j--)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
            }
        }
    }
}
