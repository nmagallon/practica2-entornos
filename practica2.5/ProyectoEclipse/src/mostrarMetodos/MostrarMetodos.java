package mostrarMetodos;

import java.util.Scanner;
import metodos.*;

public class MostrarMetodos {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("El m�todo validarPrimo() pide un n�mero y determina si es primo o no.");
		MetodosNumericos.validarPrimo(input);
		System.out.println();
		
		System.out.println("El m�todo numeroAleatorio() pide un rango de n�meros a partir del cual sacar un n�mero aleatorio.");
		MetodosNumericos.numeroAleatorio(input);
		System.out.println();
		
		System.out.println("El m�todo calcularPotencia() calcula la potencia deseada del n�mero indicado.");
		System.out.println(MetodosNumericos.calcularPotencia(5, 2));
		System.out.println();
		
		System.out.println("El m�todo binarioADecimal() calcula el valor decimal de un n�mero binario.");
		System.out.println(MetodosNumericos.binarioADecimal(input));
		System.out.println();
		
		System.out.println("El m�todo esPar() determina si un n�mero es par o no.");
		MetodosNumericos.esPar(input);
		System.out.println();
		
		System.out.println("El m�todo palabraAleatoria() pide la longitud de la palabra aleatoria para crearla.");
		MetodosString.palabraAleatoria(input);
		System.out.println();
		
		System.out.println("El m�todo invertirCadena() invierte la cadena introducida.");
		MetodosString.invertirCadena(input);
		System.out.println();
		
		System.out.println("El m�todo mostrarAbecedario() muestra el abecedario en may�sculas.");
		MetodosString.mostrarAbecedario();
		System.out.println();
		
		System.out.println("El m�todo compararCadenas() compara dos cadenas y devuelve si el resultado es true o false.");
		System.out.println(MetodosString.compararCadenas("Hola", "Mundo"));
		System.out.println();
		
		System.out.println("El m�todo dibujarArbol() pide la altura del tri�ngulo a dibujar.");
		MetodosString.dibujarArbol(input);
		System.out.println();
		
	}

}
