package programa;

import java.util.Scanner;

public class ConsolaJava {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("*********************************"
				+ "\n" + "1 - Validar n�mero primo"
				+ "\n" + "2 - N�mero aleatorio"
				+ "\n" + "3 - Palabra aleatoria"
				+ "\n" + "4 - Invertir cadena"
				+ "\n" + "*********************************"
				+ "\n" + "Introduzca el n�mero de opci�n:");
		
		int opcion = Integer.parseInt(input.nextLine());
		
		switch(opcion) {
			case 1:
				validarPrimo(input);
				break;
			
			case 2:
				numeroAleatorio(input);
				break;
				
			case 3:
				palabraAleatoria(input);
				break;
				
			case 4:
				invertirCadena(input);
				break;
				
			default:
				System.out.println("Opci�n no contemplada.");
		}
		
		input.close();
	}
	
	public static void validarPrimo(Scanner input) {
		
		System.out.println("Introduce un n�mero entero.");
		int numeroLeido = input.nextInt();
		int contadorDivisores = 0;
		
		for (int i = 1; i <= numeroLeido; i++) {
			if (numeroLeido % i == 0) {
				contadorDivisores++;
			}
		}
		
		if (contadorDivisores == 2) {
			System.out.println("El n�mero introducido es primo.");
		} else {
			System.out.println("El n�mero introducido no es primo.");
		}
	}
	
	public static void numeroAleatorio(Scanner input) {
		
		System.out.println("Introduce el n�mero m�ximo.");
		int numeroMaximo = input.nextInt();
		System.out.println("Introduce el n�mero m�nimo.");
		int numeroMinimo = input.nextInt();
		
		System.out.println("Tu n�mero aleatorio es " + (numeroMinimo + (int)(Math.random()*(numeroMaximo - numeroMinimo))) + ".");
	}
	
	public static void palabraAleatoria(Scanner input) {
		
		System.out.println("Introduce la longitud de la palabra.");
		int longitud = input.nextInt();
		
		for (int i = 0; i < longitud; i++) {
			System.out.print((char)(Math.random()*('Z' - 'A') + 'A'));
		}
		System.out.println();
	}
	
	public static void invertirCadena(Scanner input) {
		
		System.out.println("Introduce una cadena.");
		String cadenaLeida = input.nextLine();
		
		for(int i = cadenaLeida.length() - 1; i >= 0; i--) {
			System.out.print(cadenaLeida.charAt(i));
		}
		System.out.println();
	}

}
