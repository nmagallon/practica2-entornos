﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace consolaVS_NicolasMagallon
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine(" * ********************************"
                + "\n" + "1 - Validar número primo"
                + "\n" + "2 - Número aleatorio"
                + "\n" + "3 - Palabra aleatoria"
                + "\n" + "4 - Invertir cadena"
                + "\n" + "*********************************"
                + "\n" + "Introduzca el número de opción:");
            int opcion = int.Parse(System.Console.ReadLine());
            switch (opcion)
            {
                case 1:
                    ValidarPrimo();
                    break;
                case 2:
                    NumeroAleatorio();
                    break;
                case 3:
                    PalabraAleatoria();
                    break;
                case 4:
                    InvertirCadena();
                    break;
                default:
                    System.Console.WriteLine("Opción no contemplada.");
                    break;
            }
        }

        static void ValidarPrimo()
        {
            System.Console.WriteLine("Introduce un número entero.");
            int numeroLeido = int.Parse(System.Console.ReadLine());
            int contadorDivisores = 0;
            for (int i = 1; i <= numeroLeido; i++)
            {
                if (numeroLeido % i == 0)
                {
                    contadorDivisores++;
                }
            }
            if (contadorDivisores == 2)
            {
                System.Console.WriteLine("El número introducido es primo.");
            } else
            {
                System.Console.WriteLine("El número introducido no es primo.");
            }
            Console.ReadKey();
        }

        static void NumeroAleatorio()
        {
            System.Console.WriteLine("Introduce el número máximo.");
            int numeroMaximo = int.Parse(System.Console.ReadLine());
            System.Console.WriteLine("Introduce el número mínimo.");
            int numeroMinimo = int.Parse(System.Console.ReadLine());
            Random random = new Random();
            int numeroAleatorio = random.Next(numeroMinimo, numeroMaximo);
            System.Console.WriteLine("Tu número aleatorio es " + numeroAleatorio);
            Console.ReadKey();
        }

        static void PalabraAleatoria()
        {
            System.Console.WriteLine("Introduce la longitud de la palabra.");
            int longitud = int.Parse(System.Console.ReadLine());
            Random random = new Random();
            int letraAleatoria;
            for (int i = 0; i < longitud; i++)
            {
                letraAleatoria = random.Next('A', 'Z');
                System.Console.Write((char)(letraAleatoria));
            }
            Console.ReadKey();
        }

        static void InvertirCadena()
        {
            System.Console.WriteLine("Introduce una cadena.");
            String cadenaLeida = System.Console.ReadLine();
            for (int i = cadenaLeida.Length - 1; i >= 0; i--)
            {
                System.Console.Write(cadenaLeida[i]);
            }
            Console.ReadKey();
        }
    }
}
